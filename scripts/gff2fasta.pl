#!/usr/bin/perl
use FindBin;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use Pod::Usage; 
use Cwd;
 

my $help        = "";
my $gff         = "";
my $prefix      = "";
my $fasta       = "";
my $dir         = cwd();
my $verbose     = 0;
my $get_by_name = 0;
my $current_dir = getcwd;  

my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters  
    --gff        gff3 file  [required] 
    --fasta      fasta file [required]
    --prefix     prefix for output file
    --dir        output directory (Default current directory)
    --get_by_name By default, the id for Fasta come from the ID tag, get the id from the Name tag 
    --verbose    
    --help       show this help and exit
/;
GetOptions(
    'gff=s'    => \$gff,
    'fasta=s'  => \$fasta,
    'prefix=s' => \$prefix,
    'dir=s'    => \$dir,
    'get_by_name' => \$get_by_name,
    'verbose'  => \$verbose,    
    'help|h|?' => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); } 
  
if ($gff eq "") {
    print "\nWarn :: --gff is empty. Please specify the GFF file \n\n";
    print $usage;
    exit 0;
}
if ($fasta eq "") {
    print "Warn :: --fasta is empty. Please specify the FASTA file\n";
    print $usage;
    exit 0;
}
 
 

sub print {
    my $cmd = shift;
    print LOG $cmd;
    print $cmd  if $verbose; 
}

my $output_gene    = $dir."/".$prefix .".gene.fna";
my $output_cds     = $dir."/".$prefix .".cds.fna";
my $output_cdna    = $dir."/".$prefix .".cdna.fna";
my $output_protein = $dir."/".$prefix .".protein.faa";
my $output_log     = $dir."/".$prefix.".log";
open(LOG,">$output_log"); 


# Input file
my $fasta_in = new Bio::SeqIO(
	-file => $fasta,
	-format => 'fasta'
);
my $gff_in = new Bio::Tools::GFF(
	-file => $gff,
	-gff_version => 3
);

my %is_in_list;
if ($list_gene) {
	open(IN,$list_gene);
	while(<IN>){
		chomp;
		$is_in_list{$_} = 1; 
	}
	close IN;
}
if ($list_cds){
	open(IN,$list_cds);
	while(<IN>){
		chomp;
		$is_in_list{$_} = 1; 
	}
	close IN;
}  
# Output file
###### Output type description ######
# gene 			- the entire gene sequence (including UTRs and introns)
# cds 			- translated sequence (starting with ATG and ending with a stop codon included)
# cdna 			- transcribed sequence (devoid of introns, but containing untranslated exons)
# protein 		- cds translated (includes a * as the stop codon)

my $gene_out = new Bio::SeqIO(
	-file => ">$output_gene",
	-format => 'fasta'
);
my $cds_out = new Bio::SeqIO(
	-file => ">$output_cds",
	-format => 'fasta'
);
my $cdna_out = new Bio::SeqIO(
	-file => ">$output_cdna",
	-format => 'fasta'
);
my $protein_out = new Bio::SeqIO(
	-file => ">$output_protein",
	-format => 'fasta'
);

# Read FASTA
 
my %fasta;
&print("# Read reference fasta file\n");
while (my $seqobj = $fasta_in->next_seq) {
	$fasta{$seqobj->display_id} = $seqobj;
}
$fasta_in->close;


# Read & parse GFF3
my %gene;
my %mrna;
my %exon;
my $cpt;
my %cds;
my %desc;
my %alias;

&print("# Read gff3 file\n");

while(my $feature = $gff_in->next_feature) {
	if ($feature->primary_tag() eq "gene") {
        	my ($gene_id) = $feature->get_tag_values("Name");
		if ($list_gene) { 
	        	if (defined $is_in_list{$gene_id}) {
				push @{$gene{$feature->seq_id}} , $feature;
			} 
        	}
        	else {
			push @{$gene{$feature->seq_id}} , $feature;
		}
	}
	elsif ($feature->primary_tag() eq "mRNA") {
        	my ($mrna_id) = $feature->get_tag_values("ID");
	        my $mrna_name_id;
        	if ($feature->has_tag("Name")){
	            ($mrna_name_id) = $feature->get_tag_values("Name");
        	}
	        else {
        	    $mrna_name_id = $mrna_id;
	            $feature->add_tag_value("Name",$mrna_name_id);
	        }
		my ($gene_id) = $feature->get_tag_values("Parent");
        	$alias{$mrna_id} = $mrna_name_id; 
	        $mrna{$gene_id}{$mrna_id} = $feature;
	}
	elsif ($feature->primary_tag() eq "CDS") {
		my ($mrna_id) = $feature->get_tag_values("Parent");
        	#$mrna_id = $get_by_name ? $alias{$mrna_id} : $mrna_id;
		if ($list_cds) {
		        if (defined $is_in_list{$mrna_id}) {
				push @{$cds{$mrna_id}} , $feature; 
			}
        	}
	        else {
	   		$cpt++;
			push @{$cds{$mrna_id}} , $feature;
		}
	} 
	elsif ($feature->primary_tag() eq "exon") {
		my ($mrna_id) = $feature->get_tag_values("Parent");
	        #$mrna_id = $get_by_name ? $alias{$mrna_id} : $mrna_id;
		if ($list_cds) {
            		if (defined $is_in_list{$mrna_id}) {
				push @{$exon{$mrna_id}} , $feature;
			}
        	}
	        else {
			push @{$exon{$mrna_id}} , $feature;
		}
	}
} 
$gff_in->close; 
# Write Fasta
my $cpt_gene    = 0;
my $cpt_cds     = 0;
my $start_codon = 0;
my @start;
my $stop_codon  = 0;
my @stop;
my @bad_sequence;
foreach my $seq_id (keys %fasta) {
	my $seqobj = $fasta{$seq_id}; 
	foreach my $feature (sort {$a->start <=> $b->start} @{$gene{$seq_id}}){
		$cpt_gene++;
        	my ($gene_id) = $feature->get_tag_values("ID");
		my ($gene_name) = $feature->get_tag_values("Name");
        	my $note;
		if ($feature->has_tag("Note")){
			($note) = $feature->get_tag_values("Note");
		}
		print join("\t",$gene_id,$gene_name,$note),"\n";
		my $seqobj_gene = $seqobj->trunc($feature->start,$feature->end);
		$seqobj_gene->display_id($gene_name);
		$seqobj_gene->desc($note);
		my $strand = $feature->strand;
	        if ($strand =~ /-/) {
        	    $seqobj_gene = $seqobj_gene->revcom();
	        } 
		$gene_out->write_seq($seqobj_gene);
        	foreach my $mrna_id (keys %{$mrna{$gene_id}}){
	            $cpt_cds++;
        	    my $prot_id =$alias{$mrna_id};
	            my $cds_id = $alias{$mrna_id};		 
        	    my @cds = $strand =~ /-/ ? sort {$b->start <=> $a->start} @{$cds{$mrna_id}} : sort {$a->start <=> $b->start} @{$cds{$mrna_id}};
	            my @exon = @{$exon{$mrna_id}};
        	    my $cds_seq;
	            my $feature_mrna = $mrna{$gene_id}{$mrna_id}; 
            
	            if (@cds){
        	        foreach my $feature_cds (sort{$a->start <=> $b->start} @{$cds{$mrna_id}}) { 
                	    if ($feature_cds->start >= $feature_cds->end){
                        	print "probleme = " ,$mrna_id,"\n";
	                        next;
        	            }
                	    else {
                        	$cds_seq .= $seqobj->subseq($feature_cds->start,$feature_cds->end);  
	                        my $subseq = $seqobj->subseq($feature_cds->start,$feature_cds->end); 
        	            }
                	} 
	                if ($cds_seq) {
        	            my $seqobj_cds = Bio::PrimarySeq->new(
                	        -seq        => $cds_seq, 
                        	-display_id => $cds_id
	                    ); 
        	            if ($strand =~ /-/) {
                	        $seqobj_cds = $seqobj_cds->revcom();
	                    }	
        	            if ($seqobj_cds->seq =~ /^ATG.*/){
                	        $start_codon++;
                        	push @start, $mrna_id; 
	                    } 
        	            if ($seqobj_cds->seq =~ /.*(TAG|TAA|TGA)$/){
                	        $stop_codon++;
                        	push @stop , $mrna_id; 
	                    }   
        	            my $seqobj_protein1 = $seqobj_cds->translate(-offset=>1);
                	    my $seqobj_protein2 = $seqobj_cds->translate(-offset=>2);
	                    my $seqobj_protein3 = $seqobj_cds->translate(-offset=>3);  
        	            $seqobj_protein1->display_id($prot_id);
                	    $seqobj_protein2->display_id($prot_id);
	                    $seqobj_protein3->display_id($prot_id); 
        	            my @seq_prot1 = (split(/\*/,$seqobj_protein1->seq()));
                	    my @seq_prot2 = (split(/\*/,$seqobj_protein2->seq()));
	                    my @seq_prot3 = (split(/\*/,$seqobj_protein3->seq()));
        	            my $seq_protein; 
                	    if (scalar(@seq_prot1) <= 1) {
                        	$seq_protein = $seqobj_protein1;
	                    }
        	            elsif (scalar(@seq_prot2) <=1 ) {
          
	                        $seq_protein = $seqobj_protein2;
        	            }
                	    elsif (scalar(@seq_prot3) <= 1) {
                        
	                        $seq_protein = $seqobj_protein3;
        	            }
	                    else {
        	                print $mrna_id,"\n";
	                    }
        	            if ($seq_protein) { 
                	        $seqobj_cds->desc($note);
				$cds_out->write_seq($seqobj_cds);
                        	$protein_out->write_seq($seq_protein);
	                        my $cdna_seq;
        	                foreach my $feature (sort{$a->start <=> $b->start} @{$exon{$mrna_id}}) {	
                	            $cdna_seq .= $seqobj->subseq($feature->start,$feature->end); 
                        	}	
	                        my $seqobj_cdna = Bio::PrimarySeq->new(
        	                    -seq        => $cdna_seq, 
                	            -display_id => $cds_id
                        	);
	                        if ($strand =~ /-/) {
        	                    $seqobj_cdna = $seqobj_cdna->revcom();
                	        }
				$seqobj_cdna->desc($note);
                        	$cdna_out->write_seq($seqobj_cdna);
	                    } 
        	        }
	            }
        	    else {
                	print $mrna_id ,"\twithout cds!\n";
	            }
        	}
	    }
	}
	$gene_out->close;
	$cds_out->close;
	$cdna_out->close;
	$protein_out->close;
	&print("Number of gene(s) : ". $cpt_gene ."\n");
	&print("Number of transcript : " . $cpt_cds ." (with ATG ". $start_codon ."; with stop_codon ". $stop_codon .")\n"); 
 
