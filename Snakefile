import pandas as pd 
import re
import os
import copy
import snakemake
from snakemake.io import expand, multiext, glob_wildcards, directory
from Bio import SearchIO
from snakemake.utils import validate

configfile: "config/config.yaml"

# Globals ---------------------------------------------------------------------

blast_databases_annot = config["blast_databases"] 
hmm_databases = config["hmm_databases"]
blast_databases_repeat = config["blast_repeat_database"]

### Merge all the blast databases (repeat or not) in one structure
blast_databases = copy.deepcopy(config["blast_databases"])

if config["only_reannotate"] == 0:
    blast_databases.update(blast_databases_repeat)

blast_threads=config["params"]["global_blast_threads"]

###Indices for the fasta splited files, in the form of '001', '002'...
my_indices=[]
fasta_split_number=config["fasta_split_number"]
for x in range(1,fasta_split_number+1): ##Because the last value is exclude. If it's a 4, the range will stop to 3.
    my_indices.append('%03d' % x)

input_genome=config["input_genomes"]


wildcard_constraints:
    sample= "|".join(input_genome.keys()),
    db_name = "|".join(blast_databases.keys())



###Take the output of hmmsearch, a pvalue cutoff, and return a pandas df with matching genes in query and their associated evalue.
def parse_hmmsearch_results(hmm_file, pvalue_cutoff):
    list_evalue = []
    list_id = []
    print("Starting")
    print(hmm_file)
    for qresult in SearchIO.parse(hmm_file, 'hmmer3-text'):
        hits = qresult.hits
        print("Hits...")
        print(hits)
        if len(hits) > 0:
            for i in range(0,len(hits)):
                if hits[i].evalue < pvalue_cutoff:
                    hit_evalue = hits[i].evalue
                    hit_id = hits[i].id
                    list_evalue.append(hit_evalue)
                    list_id.append(hit_id)
    return(pd.DataFrame({'evalue': list_evalue}, index=list_id))



rule all:
    input:
        expand("output/{sample}_report.html", sample=input_genome.keys()),
        expand("output/{sample}.protein.faa", sample=input_genome.keys()),
        expand("output/{sample}.gff3.gz", sample=input_genome.keys()),
        expand("output/{sample}.cds.fna.fai",sample=input_genome.keys()),
        expand("output/{sample}.gene.fna",sample=input_genome.keys()),


rule filter_longest_transcript:
    input:
        gff3 = lambda wildcards: input_genome[wildcards.sample]["gff3"],
        protein = lambda wildcards: input_genome[wildcards.sample]["protein"]
    output:
        fasta = "input/{sample}.faa",
        log = "input/{sample}.log"
    params:
        prefix = "{sample}",
        tag = config["tag"]
    envmodules:
        config["modules"]["perllib"]
    shell:"""
        scripts/filter_longest_orf.pl -gff {input.gff3} -fasta {input.protein} -prefix {params.prefix} -tag {params.tag} -outdir input
    """
        

rule genome_fasta_symlink:
    input:
        lambda wildcards: input_genome[wildcards.sample]["assembly"]
    output:
        fasta = "input/{sample}_genome.fasta"
    shell:
        "ln -nrfs {input} {output}"

###########################################
### Create fasta index (used to get sequence length)
rule fasta_index:
    input:
        "input/{sample}.faa"
    output:
        "input/{sample}.faa.fai"
    envmodules:
        config["modules"]["samtools"]
    shell:
        "samtools faidx --fai-idx {output} {input} "


if config["only_reannotate"]:
    rule report_only_annotate:
        input:
            fasta_index=rules.fasta_index.output,
            iss_protein="results/{sample}_protein.iss"
        output:
            "output/{sample}_report.html"
        envmodules:
            config["modules"]["R"]
        script:
            "pipeline_report.Rmd"
else:
    rule report_reannotation:
        input:
            fasta_index=rules.fasta_index.output,
            iss_protein="results/{sample}_protein.iss",
            iss_te="results/{sample}_TE.iss",
            filter_summary="results/{sample}_summary.tsv" 
        output:
            "output/{sample}_report.html"
        envmodules:
            config["modules"]["R"]
        params:
            to_remove_string="results/{sample}_to_remove.tsv",
            filter_summary="results/{sample}_summary.tsv"
        script:
            "pipeline_report.Rmd"

####################################
### Split files in n pieces
rule split:
    input:
        "input/{sample}.faa"
    output:
        temp(expand("temp/{{sample}}/{{sample}}.part_{n}.faa", n=my_indices))
    params:
        number_of_output_fasta=fasta_split_number
    envmodules:
        config["modules"]["seqkit"]
    shell:
        "seqkit split --out-dir temp/{wildcards.sample} --by-part {params.number_of_output_fasta} {input}"

####################################
### Blast Part

rule build_blast_databases:
    input:
        lambda wildcards: blast_databases[wildcards.db_name]
    output:
        done=touch("results/blast_databases/{db_name}.makeblastdb.done")
    envmodules:
        config["modules"]["blast"]
    shell:
        "makeblastdb -parse_seqids -in {input} -dbtype prot  -out results/blast_databases/{wildcards.db_name} "

rule build_diamond_database:
    input:
        blast_db="results/blast_databases/{db_name}.makeblastdb.done"
    output:
        done=touch("results/blast_databases/{db_name}.diamonddb.done")
    envmodules:
        config["modules"]["diamond"]
    shell:
        "diamond prepdb -d results/blast_databases/{wildcards.db_name} "


rule diamond_analysis:
    input:
        fasta_files="temp/{sample}/{sample}.part_{indice}.faa",
        blast_db="results/blast_databases/{db_name}.diamonddb.done"
    output:
        temp("results/raw_diamond_output/{db_name}_file_{sample}.part_{indice}.tsv")
    envmodules:
        config["modules"]["diamond"]
    params:
        evalue=1e-10,
        max_target_seqs=2,
        mode="--very-sensitive",
        output_format="--outfmt 6 qseqid qlen sseqid slen stitle evalue pident qcovhsp scovhsp  sstart send length"
    threads:
        config["params"]["global_diamond_threads"]
    shell:
        "diamond blastp  --masking 0 --unal 1 {params.output_format}  {params.mode} --threads {threads}  --db results/blast_databases/{wildcards.db_name} --query {input.fasta_files} --evalue {params.evalue} -k {params.max_target_seqs}  -o {output} "

rule blastp_computation:
    input:
        fasta_files="temp/{sample}/{sample}.part_{indice}.faa",
        blast_db="results/blast_databases/{db_name}.makeblastdb.done"
    output:
        temp("results/raw_blast_output/{db_name}_file_{sample}.part_{indice}.tsv")
    params:
        evalue=1e-10,
        max_target_seqs=2,
        output_format="-outfmt '6 qseqid qlen sseqid slen stitle evalue pident qcovhsp scovhsp  sstart send length'"
    envmodules:
        config["modules"]["blast"]
    threads:
        config["params"]["global_blast_threads"]
    shell:
        "blastp -db results/blast_databases/{wildcards.db_name} {params.output_format} -query {input.fasta_files} -out {output} -num_threads {threads} -evalue {params.evalue} -max_target_seqs {params.max_target_seqs}"


rule merge_blast_results:
    input:
        expand("results/raw_blast_output/{{db_name}}_file_{{sample}}.part_{indice}.tsv", indice=my_indices) if config["use_diamond"] == 0 else  expand("results/raw_diamond_output/{{db_name}}_file_{{sample}}.part_{indice}.tsv", indice=my_indices)
    output:
        "results/{db_name}_{sample}.tsv"
    shell:
        "cat {input} > {output}"

####################################
### Compute ISS

###TODO : Need to find which ISS is the best before going to the filtering part. We need to have like one file "Best ISS prot" and one other "Best ISS TE"
rule compute_ISS:
    input:
        db_repeat=expand("results/{db_name}_{{sample}}.tsv", db_name=blast_databases_annot.keys())
    output:
        protein="results/{sample}_protein.iss"
    params:
        db_annotation=' --blast '.join(expand("results/{db_name}_{{sample}}.tsv",db_name=blast_databases_annot.keys()))
    envmodules:
        config["modules"]["perllib"]
    shell:
        "scripts/cnv_blast.pl --blast {params.db_annotation} --out {output.protein}  "

if config["only_reannotate"]==0:
    rule compute_ISS_TE:
        input:
            db_repeat=expand("results/{db_name}_{{sample}}.tsv", db_name=blast_databases_repeat.keys())
        output:
            "results/{sample}_TE.iss"
        params:
            db_repeat_TE=' --blast '.join(expand("results/{db_name}_{{sample}}.tsv",db_name=blast_databases_repeat.keys()))
        envmodules:
            config["modules"]["perllib"]
        shell:
            "scripts/cnv_blast.pl  --blast {params.db_repeat_TE} --out {output} "

    ###TODO : Add length information to this file to improve filtering.
    ###TODO : this can be skip, just read the files in a python-like rule and merge them with python.
    rule merge_iss:
        input:
            proteins="results/{sample}_protein.iss",
            TE="results/{sample}_TE.iss" if config["only_reannotate"]==0 else " "
        output:
            "results/{sample}.iss"
        shell:
            "join -t $'\t' {input.proteins} {input.TE} > {output} "

####################################
### Interpro Part

rule interpro_scan:
    input:
        fasta_files = "temp/{sample}/{sample}.part_{indice}.faa",
    output:
        "results/interproscan/{sample}/{sample}.part_{indice}.tsv"
    threads: config["params"]["interproscan_threads"]
    envmodules:
        config["modules"]["interproscan"]
    shell:"""
        interproscan.sh -cpu {threads} --iprlookup --goterms -f tsv -T ./temp/  --disable-precalc -i {input} --output-file-base results/interproscan/{wildcards.sample}/{wildcards.sample}.part_{wildcards.indice}
    """

rule kofamscan:
    input:
        "temp/{sample}/{sample}.part_{indice}.faa",
    output:
        temp("results/kofamscan/{sample}.part_{indice}.txt")
    envmodules:
        config["modules"]["kofamscan"]
    params:
        tmp = "temp/{sample}.part_{indice}",
        threads = config["params"]["kofamscan_threads"]
    shell:"""
        exec_annotation --cpu {params.threads} --tmp-dir {params.tmp} --format mapper -o {output} {input}
    """

rule merge_kofamscan:
    input:
        expand("results/kofamscan/{{sample}}.part_{indice}.txt", indice=my_indices)
    output:
        "results/{sample}_ko.txt"
    shell:
        "cat {input} > {output}"




rule enrichment:
    input:
        expand("results/interproscan/{{sample}}/{{sample}}.part_{indice}.tsv", indice=my_indices),
        product = "results/{sample}_protein.iss",
        ko="results/{sample}_ko.txt"
    output:
        go="output/{sample}_go.txt",
        ipr="output/{sample}_ipr.txt",
        ec="output/{sample}_ec.txt",
        dbxref="output/{sample}_dbxref.txt",
        ko="output/{sample}_ko.txt"
    params:
        output_prefix="{sample}",
        output_interpro="results/interproscan/{sample}",
        output_directory="output/"
    envmodules:
        config["modules"]["perllib"]
    shell:"""
        scripts/enrichment.pl --result {params.output_interpro} --ko {input.ko} --product {input.product} --prefix {params.output_prefix} --outdir {params.output_directory}
    """

    
rule add_annotation_to_gff3:
    input:
        iss = rules.compute_ISS.output,
        gene_ontology = rules.enrichment.output.go,
        interpro = rules.enrichment.output.ipr,
        ec = rules.enrichment.output.ec,
        dbxref = rules.enrichment.output.dbxref,
        ko = rules.enrichment.output.ko,
        gff3 = lambda wildcards: input_genome[wildcards.sample]["gff3"]
    output:
        gff3 = temp("output/{sample}_temp.gff3")
    envmodules:
        config["modules"]["perllib"]
    params:
        tag               = config["tag"],
        to_remove_string  = " -te_file results/{sample}_to_remove.tsv " if config["only_reannotate"]==0 else  "",
        to_rename_gene_id = " -rename " if config["rename_gene_id"]==1 else ""
    shell:"""
        scripts/cnv_eugene_gff3.pl \
            -product {input.iss} \
            {params.to_remove_string} \
            {params.to_rename_gene_id} \
            -go_file {input.gene_ontology} \
            -interpro_file {input.interpro} \
            -gff3_file {input.gff3} \
            -ec_file {input.ec} \
            -ko_file {input.ko} \
            -dbxref_file {input.dbxref} \
            -prefix output/{wildcards.sample} \
            -tag {params.tag}
    """
    
# GFF3 need to be prperly sorted for indexing with tabix
rule gff3sort:
    input:
        rules.add_annotation_to_gff3.output.gff3
    output:
        "output/{sample}.gff3"
    envmodules:
        config["modules"]["perllib"]
    shell:"""
        scripts/gff3sort.pl --precise {input} > {output};
    """

# Indexing GFF before integration on JBrowse
rule tabix:
    input:
        rules.gff3sort.output
    output:
        gz = "output/{sample}.gff3.gz",
        tbi = "output/{sample}.gff3.gz.tbi"
    envmodules:
        config["modules"]["tabix"]
    shell:"""
       bgzip -c {input} > {output.gz};
       tabix -p gff {output.gz};
    """
rule gff2fasta:
    input:
        gff3 = rules.gff3sort.output,
        fasta = rules.genome_fasta_symlink.output.fasta
    output:
        gene = "output/{sample}.gene.fna",
        protein="output/{sample}.protein.faa",
        cds="output/{sample}.cds.fna",
	cdna="output/{sample}.cdna.fna"
    params:
        tag    = config["tag"],
        prefix = "{sample}"
    envmodules:
        config["modules"]["perllib"]
    shell:"""
        scripts/gff2fasta.pl --gff {input.gff3} --fasta {input.fasta} -prefix {params.prefix}  --get_by_name  {params.tag} --dir output
    """

rule index_extracted_sequences:
    input:
        protein=rules.gff2fasta.output.protein,
        cds=rules.gff2fasta.output.cds,
        gene=rules.gff2fasta.output.gene,
	cdna=rules.gff2fasta.output.cdna
    output:
        proteins="output/{sample}.protein.faa.fai",
        cds="output/{sample}.cds.fna.fai",
        gene="output/{sample}.gene.fna.fai",
    envmodules:
        config["modules"]["samtools"] 
    shell:"""
        samtools faidx {input.gene};
        samtools faidx {input.cds};
        samtools faidx {input.protein};
	    samtools faidx {input.cdna}
    """
    

###This is some kind of implementation of Laila script.
rule ISS_table_creation_and_purification:
    input:
        iss_full="results/{sample}.iss",
        interpro ="output/{sample}_ipr.txt",
        fasta_index="input/{sample}.faa.fai",
        merged_hmm="results/{sample}_hmm.tsv",
        busco_match="results/{sample}_busco.txt"
    output:
        filter_summary="results/{sample}_summary.tsv",
        genes_to_remove="results/{sample}_to_remove.tsv"
    params:
        ipro_term_to_remove="|".join(config["ipro_term_to_remove"]),
        blast_keyword_to_remove="|".join(config["keywork_to_remove"])
    run:
        # ipdb.set_trace()
        raw_iss_table = pd.read_table(input.iss_full, index_col=0,  names=["gene_name", "subject_prot", "results_prot",
                                                                           "length_prot", "annotation_prot",  "ISS_prot",
                                                                           "completness_prot", "gene_prot", "dbxref_prot",
                                                                           "subject_TE", "results_TE", "length_TE",
                                                                           "annotation_TE",  "ISS_TE", "completness_TE",
                                                                           "gene_TE", "dbxref_TE"])
        # raw_iss_table = pd.read_table(input.iss_full, index_col=0,  names=["gene_name", "annotation_prot", "ISS_prot", "completness_prot", "gene_prot", "other_prot", "annotation_TE", "ISS_TE", "completness_TE", "gene_TE", "other_TE"])
        ###Add length informations.
        protein_length = pd.read_table(
            input.fasta_index,index_col=0,usecols=[0, 1],low_memory=True,names=["gene_name", "protein_length",
                                                                                      "usless_1", "usless_2",
                                                                                      "usless_3"])
        raw_iss_table = raw_iss_table.join(protein_length,on="gene_name")

        ###Add interpro informations.
        cnv_interpro_table = pd.read_table(input.interpro,names=["gene_name", "ipro_id"])  ###Read file
        cnv_interpro_table = cnv_interpro_table[cnv_interpro_table["ipro_id"] != "-"]  ###Remove useless empty rows.
        cnv_interpro_table = cnv_interpro_table.groupby('gene_name')['ipro_id'].agg([('merged_interpro', ';'.join)]) ###Merge lines with same gene id


        cnv_interpro_table_merge = pd.merge(left=raw_iss_table,right=cnv_interpro_table,on="gene_name",how='outer')

        ###Add hmm evalue informations
        if os.path.getsize(input.merged_hmm):
            hmm_table = pd.read_table(input.merged_hmm, index_col=0)
            # ipdb.set_trace()
            cnv_interpro_table_merge = cnv_interpro_table_merge.join(hmm_table, how="outer")

        ###Calculation of which proteins to remove
        #Filter 1 : remove all ISS_6
        removed_ISS6 = cnv_interpro_table_merge["ISS_prot"] == "ISS_6"
        
        #Filter 2 : Remove ISS_5 with a length < 150AA
        removed_ISS5_small = (cnv_interpro_table_merge["ISS_prot"] == "ISS_5") & (
                    cnv_interpro_table_merge["protein_length"] < 150)
        
        #Filter 3 : Remove if ISS_TE is greater than ISS not TE (TE 2 3 4) & (NTE 4 5)
        removed_ISS4_5_below_TE = (cnv_interpro_table_merge["ISS_TE"].isin(["ISS_2", "ISS_3", "ISS_4"])) & (
            cnv_interpro_table_merge["ISS_prot"].isin(["ISS_5", "ISS4"]))
        
        #Filter 4 : remove fragmented ISS_3 if the corresponding ISS_TE is 2 or 3.
        removed_incomplete_ISS3_below_TE = (cnv_interpro_table_merge["ISS_TE"].isin(["ISS_2", "ISS_3"])) & (
                    (cnv_interpro_table_merge["ISS_prot"] == "ISS_3") & (
                        cnv_interpro_table_merge["completness_prot"] == 'fragment'))
        
        #Filter 5 : Remove a list of keyword associated with unwanted stuff.
        removed_TE_keyword = cnv_interpro_table_merge["annotation_prot"].str.contains(params.blast_keyword_to_remove,case=False,na=False)
        
        #Filter 6 : remove prots containing some interpro terms if they are not "modules".
        remove_TE_interpro = (cnv_interpro_table_merge[
                                  "merged_interpro"].str.contains(params.ipro_term_to_remove,case=False,na=False)) & (
                                 ~cnv_interpro_table_merge[
                                     'completness_prot'].str.contains('modules',case=False,na=False))

        #Fitler 7 : Remove IPR018289 when not associated with IPR031052 and module.
        remove_MULE = (cnv_interpro_table_merge["merged_interpro"].str.contains("IPR018289",case=False,na=False)) &(
                                ~cnv_interpro_table_merge["merged_interpro"].str.contains("IPR031052",case=False,na=False)) &(
                                ~cnv_interpro_table_merge['completness_prot'].str.contains('modules',case=False,na=False))

        #Filter 8 : Remove IPR000477 when not associated with IPR024937 or IPR003545 and not module.
        remove_RT = (cnv_interpro_table_merge["merged_interpro"].str.contains("IPR000477",case=False,na=False)) &(
                                ~cnv_interpro_table_merge["merged_interpro"].str.contains("IPR024937|IPR003545",case=False,na=False)) &(
                                ~cnv_interpro_table_merge['completness_prot'].str.contains('modules',case=False,na=False))



        ###Add a col with True when a busco is found
        busco_hit = pd.read_table(input.busco_match,header=None,index_col=0)
        busco_hit["is_busco"] = True
        cnv_interpro_table_merge = cnv_interpro_table_merge.join(busco_hit,how="outer")
        cnv_interpro_table_merge["is_busco"] = cnv_interpro_table_merge["is_busco"].fillna(False)
        busco_to_keep = cnv_interpro_table_merge["is_busco"]
        #cnv_interpro_table_merge.to_csv(output.ISS_raw, sep="\t")
 



        if os.path.getsize(input.merged_hmm):
            any_hmm_pvalue = ~(cnv_interpro_table_merge.filter(regex="evalue_.*").isnull()).any(axis=1)
            to_remove = (removed_ISS6 | removed_ISS5_small | removed_ISS4_5_below_TE | removed_incomplete_ISS3_below_TE | removed_TE_keyword | remove_TE_interpro | remove_MULE | remove_RT | any_hmm_pvalue)  & ~busco_to_keep
            filter_summary = pd.concat([removed_ISS6, removed_ISS5_small, removed_ISS4_5_below_TE, removed_incomplete_ISS3_below_TE, removed_TE_keyword, remove_TE_interpro, remove_MULE, remove_RT, busco_to_keep],axis=1)
            filter_summary.columns=["ISS 6", "Small ISS 5", "ISS 4-5 and TE", "Icomplete ISS3 TE", "TE Keyword", "TE Interpro", "MULE", "RT", "is_busco"]
            filter_summary = pd.concat([filter_summary,   ~(cnv_interpro_table_merge.filter(regex="evalue_.*").isnull())], axis=1)
            filter_summary.loc['Column_Total'] = filter_summary.sum(numeric_only=True,axis=0)
        else:
            to_remove = (removed_ISS6 | removed_ISS5_small | removed_ISS4_5_below_TE | removed_incomplete_ISS3_below_TE | removed_TE_keyword | remove_TE_interpro | remove_MULE | remove_RT)  & ~busco_to_keep
            filter_summary = pd.concat([removed_ISS6, removed_ISS5_small, removed_ISS4_5_below_TE, removed_incomplete_ISS3_below_TE, removed_TE_keyword, remove_TE_interpro, remove_MULE, remove_RT, busco_to_keep], axis=1)
            filter_summary.columns = ["ISS 6", "Small ISS 5", "ISS 4-5 and TE", "Icomplete ISS3 TE", "TE Keyword", "TE Interpro", "MULE", "RT", "is_busco"]
            filter_summary.loc['Column_Total'] = filter_summary.sum(numeric_only=True,axis=0)

      
        #cnv_interpro_table_merge[to_remove].to_csv(output.ISS_final_removed, sep="\t")
        #cnv_interpro_table_merge = cnv_interpro_table_merge[~to_remove]
        #cnv_interpro_table_merge.to_csv(output.ISS_final, sep="\t")
        filter_summary.to_csv(output.filter_summary, sep="\t")

        to_remove[to_remove].to_csv(output.genes_to_remove,columns=[],header=False)

      


###FIXME : useless to split, hmmsearch is very fast in our case because very few input sequences.
rule hmm_search:
    input:
        fasta_files= "input/{sample}.faa",
        hmm_file=lambda wildcards: hmm_databases[wildcards.hmm_db_name]
    output:
        temp("results/{sample}_{hmm_db_name}.txt")
    envmodules:
        config["modules"]["hmmer"]
    shell:
        "hmmsearch {input.hmm_file} {input.fasta_files} > {output}"

rule hmm_parse:
    input:
        hmm_file="results/{sample}_{hmm_db_name}.txt"
    output:
        parsed_hmm=temp("results/{sample}_{hmm_db_name}_parsed.tsv")
    run:
        dataframe = parse_hmmsearch_results(input.hmm_file, 0.01)
        dataframe = dataframe.add_suffix("_"+wildcards.hmm_db_name)
        dataframe.to_csv(output.parsed_hmm, sep="\t")


rule merge_hmm_results:
    input:
        parsed_hmm=expand("results/{{sample}}_{hmm_db}_parsed.tsv", hmm_db = hmm_databases)
    output:
        merged_hmm="results/{sample}_hmm.tsv"
    run:
        dfs = []
        # ipdb.set_trace()
        for i in input.parsed_hmm:
            df = pd.read_table(i, index_col=0)
            if(len(df) >= 1):
                dfs = df.join(dfs, how = "outer")
        if(len(dfs) > 1):
            print('Multime HMM results')
            dfs.to_csv(output.merged_hmm, sep="\t", na_rep = "NaN")
        if(len(dfs) == 1):
            print('Single HMM results')
            dfs[0].to_csv(output.merged_hmm, sep="\t")
        else:
            print("Only one HMM result... Sad.")
            open(output.merged_hmm,'a').close() ###If the file is empty


rule busco_input:
    input:
        "input/{sample}.faa"
    output:
        touch("results/busco_input/{sample}/done")
    threads:
        config["params"]["busco_threads"]
    envmodules:
        config["modules"]["busco"]
    params:
         lineage=config["lineage"],
         busco_path=config["busco_path"]
    shell: "busco -i {input} -f -m prot -l {params.lineage} --offline -c {threads} --download_path {params.busco_path} --out_path results/busco_input/ -o {wildcards.sample}"

rule extract_busco_match:
    input:
        "results/busco_input/{sample}/done"
    output:
        "results/{sample}_busco.txt"
    params:
         lineage=config["lineage"],
    shell:
        "grep -v \"^#\" results/busco_input/{wildcards.sample}/run_{params.lineage}/full_table.tsv | cut -f3 | sort -u | grep -v \"^$\"  > {output}"

rule busco_output:
    input:
        rules.gff2fasta.output.protein
    output:
        touch("results/busco_output/{sample}/done")
    threads:
        config["params"]["busco_threads"]
    envmodules:
        config["modules"]["busco"]
    params:
         lineage=config["lineage"],
         busco_path=config["busco_path"]
    shell: "busco -i {input} -f -m prot -l {params.lineage} --offline -c {threads} --download_path {params.busco_path} --out_path results/busco_output/ -o {wildcards.sample}"

