#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use FindBin;
use Pod::Usage;
use Bio::SeqIO;
use Data::Dumper; 
use Bio::Tools::GFF;
use Bio::SeqIO;
my $gff = ""; 
my $fasta = "";
my $outdir = ""; 
my $prefix = ""; 
my $help;
my $tag = "Name";  #id separator, Allowed values are: ID, Name, locus_tag

my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -gff        GFF3 file 
    -fasta 		prot fasta
    -prefix     prefix to replace
    -outdir     outdir 
    -help		print help
    -tag		id separator, defalt Name, allowed values are: ID, Name, locus_tag
/;
GetOptions(
    'gff=s'    => \$gff, 
    'fasta=s'  => \$fasta,
    'prefix=s' => \$prefix,
    'outdir=s' => \$outdir, 
    'help|h|?' => \$help,
    'tag=s' => \$tag
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }

# Open file
my $in = new Bio::Tools::GFF(
    -file => $gff,
    -gff_version => 3
);
my $fasta_update = $outdir  ."/".$prefix.".faa";
my $logfile = $outdir  ."/".$prefix.".log";
my $infasta = new Bio::SeqIO(
    -file => $fasta,
    -format => 'fasta'
);
my $outfasta = new Bio::SeqIO(
    -file => ">$fasta_update",
    -format => 'fasta'
);

## Check that tag is either name or id
if ($tag ne "Name" && $tag ne "ID"){
	pod2usage(-message => "Error: tag must be either Name or ID. ", -verbose => 0);
}

my %fasta;
while(my $seqobj = $infasta->next_seq){
    my $display_id = $seqobj->display_id; 
    $fasta{$display_id} = $seqobj;
}
$infasta->close;
  
my %exist;
my  $cpt = 0;
open(LOG,">$logfile");
while(my $feature = $in->next_feature) { 
    if ($feature->primary_tag eq "mRNA") {
        my ($mrna_id) = $feature->get_tag_values("$tag");
        my ($parent) = $feature->get_tag_values("Parent");
        $parent =~ s/gene://;
        if ($fasta{$mrna_id} ) {
            if ($exist{$parent}) {
                if ($fasta{$mrna_id}->length > $exist{$parent}->length){
                    $exist{$parent} = $fasta{$mrna_id};
                }
            }
            else {
                $exist{$parent} = $fasta{$mrna_id};
            }
            
        }
        else {
            print LOG $mrna_id ,"\tnot found\n";
        }
        
    }
} 
$in->close;
close LOG;
foreach my $id (keys %exist) {
    $exist{$id}->display_id($id);
    $outfasta->write_seq($exist{$id});
}

