#!/bin/python

import argparse
import os
import pandas as pd 


# iss_full="results/{sample}.iss",
# interpro ="output/{sample}_ipr.txt",
# fasta_index="input/{sample}.faa.fai",
# merged_hmm="results/{sample}_hmm.tsv",
# busco_match="results/{sample}_busco.txt"

def is_file(path):
    """Check if the path is an existing file."""
    if os.path.isfile(path):
        return path
    else:
        # exit(path + " does not exist.")
        raise argparse.ArgumentTypeError(f"File {path} does not exist.")


def parse_arguments():
    parser = argparse.ArgumentParser(
                        prog='create_iss_table',
                        description='This program creates iss table. (really)',
                        epilog='Contact : alexandre.soriano@cirad.fr')

    # Input files with different kind of results
    parser.add_argument("--iss_table", required=True, type=is_file, help="Path to the ISS table file (must exist).")
    parser.add_argument("--interpro_table", required=True, type=is_file, help="Path to the InterPro table file (must exist).")
    parser.add_argument("--fasta_index", required=True, type=is_file, help="Path to the FASTA index file (must exist).")
    parser.add_argument("--busco_match", required=True, type=is_file, help="Path to the BUSCO match file (must exist).")

    # Input file with list of terms to look for
    parser.add_argument("--interpro_to_remove", required=True, type=is_file, help="Path to the interpro to remove file (must exist).")
    parser.add_argument("--blast_keyword_to_remove", required=True, type=is_file, help="Path to the blast keyword to remove file (must exist).")

    # Arbitrary files
    parser.add_argument("--extra_files", nargs='+', type=is_file, help="Additional input files, with genename and pvalue (must exist).")

    # Output file prefix
    parser.add_argument("--output_prefix", required=True, help="Prefix pattern for output files.")

    # Optional argument
    parser.add_argument("--merged_hmm", type=is_file, help="Path to the merged HMM file (optional, must exist if provided).")

    return(parser.parse_args())

# Read a file, and create a string with a concatenation of all the lines separated by a "|"
def process_lists_to_remove(input_file):
    # Open the file in read mode
    with open(input_file, 'r') as file:
        # Read the lines from the file
        lines = file.readlines()

    # Join the lines with a "|" separator
    result = "|".join(line.strip() for line in lines)

    # Print the result
    return(result)

def main():

    args = parse_arguments()

    print(args.iss_table)

    interpro_terms_to_remove = process_lists_to_remove(args.interpro_to_remove)
    blast_keyword_to_remove = process_lists_to_remove(args.blast_keyword_to_remove)

    # Read raw iss table 
    raw_iss_table = pd.read_table(args.iss_table, index_col=0,  names=["gene_name", "subject_prot", "results_prot",
                                                                        "length_prot", "annotation_prot",  "ISS_prot",
                                                                        "completness_prot", "gene_prot", "dbxref_prot",
                                                                        "subject_TE", "results_TE", "length_TE",
                                                                        "annotation_TE",  "ISS_TE", "completness_TE",
                                                                        "gene_TE", "dbxref_TE"])
    



    ###Add length informations.
    protein_length = pd.read_table(
        args.fasta_index,index_col=0,usecols=[0, 1],low_memory=True,names=["gene_name", "protein_length",
                                                                                    "usless_1", "usless_2",
                                                                                    "usless_3"])
    raw_iss_table = raw_iss_table.join(protein_length,on="gene_name")

    ###Add interpro informations.
    cnv_interpro_table = pd.read_table(args.interpro_table,names=["gene_name", "ipro_id"])  ###Read file
    cnv_interpro_table = cnv_interpro_table[cnv_interpro_table["ipro_id"] != "-"]  ###Remove useless empty rows.
    cnv_interpro_table = cnv_interpro_table.groupby('gene_name')['ipro_id'].agg([('merged_interpro', ';'.join)]) ###Merge lines with same gene id


    # merge ISS table with intepro table.
    cnv_interpro_table_merge = pd.merge(left=raw_iss_table,right=cnv_interpro_table,on="gene_name",how='outer')

    ###Add hmm evalue informations
    # if args.merged_hmm and os.path.getsize(input.merged_hmm):
    #     hmm_table = pd.read_table(args.merged_hmm, index_col=0)
    #     # ipdb.set_trace()
    #     cnv_interpro_table_merge = cnv_interpro_table_merge.join(hmm_table, how="outer")

    # ipdb.set_trace()

    # Go through all arbitrary file, and create a big table with all the pvalue.
    for i in args.extra_files:
        # print(i)
        current_file = pd.read_table(i, index_col=0, low_memory=True ,names=["gene_name", "evalue "+i], float_precision="round_trip")
        cnv_interpro_table_merge = cnv_interpro_table_merge.join(current_file, how="outer")


    ##Calculation of which proteins to remove

    #Filter 1 : remove all ISS_6
    removed_ISS6 = cnv_interpro_table_merge["ISS_prot"] == "ISS_6"

    #Filter 2 : Remove ISS_5 with a length < 150AA
    removed_ISS5_small = (cnv_interpro_table_merge["ISS_prot"] == "ISS_5") & (
                cnv_interpro_table_merge["protein_length"] < 150)

    #Filter 3 : Remove if ISS_TE is greater than ISS not TE (TE 2 3 4) & (NTE 4 5)
    removed_ISS4_5_below_TE = (cnv_interpro_table_merge["ISS_TE"].isin(["ISS_2", "ISS_3", "ISS_4"])) & (
        cnv_interpro_table_merge["ISS_prot"].isin(["ISS_5", "ISS4"]))

    #Filter 4 : remove fragmented ISS_3 if the corresponding ISS_TE is 2 or 3.
    removed_incomplete_ISS3_below_TE = (cnv_interpro_table_merge["ISS_TE"].isin(["ISS_2", "ISS_3"])) & (
                (cnv_interpro_table_merge["ISS_prot"] == "ISS_3") & (
                    cnv_interpro_table_merge["completness_prot"] == 'fragment'))

    #Filter 5 : Remove a list of keyword associated with unwanted stuff.
    removed_TE_keyword = cnv_interpro_table_merge["annotation_prot"].str.contains(blast_keyword_to_remove,case=False,na=False)

    #Filter 6 : remove prots containing some interpro terms if they are not "modules".
    remove_TE_interpro = (cnv_interpro_table_merge[
                                "merged_interpro"].str.contains(interpro_terms_to_remove,case=False,na=False)) & (
                                ~cnv_interpro_table_merge[
                                    'completness_prot'].str.contains('modules',case=False,na=False))

    #Fitler 7 : Remove IPR018289 when not associated with IPR031052 and module.
    remove_MULE = (cnv_interpro_table_merge["merged_interpro"].str.contains("IPR018289",case=False,na=False)) &(
                            ~cnv_interpro_table_merge["merged_interpro"].str.contains("IPR031052",case=False,na=False)) &(
                            ~cnv_interpro_table_merge['completness_prot'].str.contains('modules',case=False,na=False))

    #Filter 8 : Remove IPR000477 when not associated with IPR024937 or IPR003545 and not module.
    remove_RT = (cnv_interpro_table_merge["merged_interpro"].str.contains("IPR000477",case=False,na=False)) &(
                            ~cnv_interpro_table_merge["merged_interpro"].str.contains("IPR024937|IPR003545",case=False,na=False)) &(
                            ~cnv_interpro_table_merge['completness_prot'].str.contains('modules',case=False,na=False))



    ###Add a col with True when a busco is found
    busco_hit = pd.read_table(args.busco_match,header=None,index_col=0)
    busco_hit["is_busco"] = True
    cnv_interpro_table_merge = cnv_interpro_table_merge.join(busco_hit,how="outer")
    # FIXME : fillna will be deprecated
    cnv_interpro_table_merge["is_busco"] = cnv_interpro_table_merge["is_busco"].fillna(False)
    busco_to_keep = cnv_interpro_table_merge["is_busco"]
    #cnv_interpro_table_merge.to_csv(output.ISS_raw, sep="\t")

    # Go through all the files with pvalue, and create a table with all of them. 
    if 1 == 0:
        print("Etrange")
        for i in args.pvalue_file_list:
            print("Pouic")


    if args.extra_files:
        # any_hmm_pvalue = ~(cnv_interpro_table_merge.filter(regex="evalue_.*").isnull()).any(axis=1)
        any_hmm_pvalue = ~(cnv_interpro_table_merge.filter(regex="evalue*").isnull()).any(axis=1)
        to_remove = (removed_ISS6 | removed_ISS5_small | removed_ISS4_5_below_TE | removed_incomplete_ISS3_below_TE | removed_TE_keyword | remove_TE_interpro | remove_MULE | remove_RT | any_hmm_pvalue)  & ~busco_to_keep
        filter_summary = pd.concat([removed_ISS6, removed_ISS5_small, removed_ISS4_5_below_TE, removed_incomplete_ISS3_below_TE, removed_TE_keyword, remove_TE_interpro, remove_MULE, remove_RT, busco_to_keep],axis=1)
        filter_summary.columns=["ISS 6", "Small ISS 5", "ISS 4-5 and TE", "Icomplete ISS3 TE", "TE Keyword", "TE Interpro", "MULE", "RT", "is_busco"]
        filter_summary = pd.concat([filter_summary,   ~(cnv_interpro_table_merge.filter(regex="evalue*").isnull())], axis=1)
        filter_summary.loc['Column_Total'] = filter_summary.sum(numeric_only=True,axis=0)
    else:
        to_remove = (removed_ISS6 | removed_ISS5_small | removed_ISS4_5_below_TE | removed_incomplete_ISS3_below_TE | removed_TE_keyword | remove_TE_interpro | remove_MULE | remove_RT)  & ~busco_to_keep
        filter_summary = pd.concat([removed_ISS6, removed_ISS5_small, removed_ISS4_5_below_TE, removed_incomplete_ISS3_below_TE, removed_TE_keyword, remove_TE_interpro, remove_MULE, remove_RT, busco_to_keep], axis=1)
        filter_summary.columns = ["ISS 6", "Small ISS 5", "ISS 4-5 and TE", "Icomplete ISS3 TE", "TE Keyword", "TE Interpro", "MULE", "RT", "is_busco"]
        filter_summary.loc['Column_Total'] = filter_summary.sum(numeric_only=True,axis=0)


    #cnv_interpro_table_merge[to_remove].to_csv(output.ISS_final_removed, sep="\t")
    #cnv_interpro_table_merge = cnv_interpro_table_merge[~to_remove]
    #cnv_interpro_table_merge.to_csv(output.ISS_final, sep="\t")

    # Write a csv files with informations about all the different filters for each genes. 0 mean that the gene pass the filte, 1 that he fails.
    filter_summary.to_csv(args.output_prefix + "_filter_summary.tsv", sep="\t")

    # txt file containing list of genes that needs to be removed based on above filters. 
    to_remove[to_remove].to_csv(args.output_prefix + "_genes_to_remove.tsv",columns=[],header=False)

if __name__ == "__main__":
    main()