# Snakemake workflow: Gene functional annotation

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)           

**Table of Contents**

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objective

Functional annotation allows adding biologically relevant information to predicted features
in genomic sequences, and it is, therefore, an important procedure of any de novo genome sequencing
project. It is also useful for proofreading and improving gene structural annotation. 

## Dependencies

* hmmsearch : Search a sequence database with a profile HMM (http://www.csb.yale.edu/userguides/seq/hmmer/docs/node26.html)
* samtools : Reading/writing/editing/indexing/viewing SAM/BAM/CRAM format (http://www.htslib.org/)
* seqkit : SeqKit - a cross-platform and ultrafast toolkit for FASTA/Q file manipulation (https://bioinf.shenwei.me/seqkit/)
* diamond : DIAMOND is a sequence aligner for protein and translated DNA searches, designed for high performance analysis of big sequence data. (https://github.com/bbuchfink/diamond)
* ncbi-blast : Basic Local Alignment Search Tool (https://blast.ncbi.nlm.nih.gov/Blast.cgi)
* interproscan : Classification of protein families (https://www.ebi.ac.uk/interpro/)
* kofamscan : KofamScan is a gene function annotation tool based on KEGG Orthology and hidden Markov model (https://github.com/takaram/kofam_scan)
* gffread : GFF/GTF utility providing format conversions, filtering, FASTA sequence extraction and more. (https://github.com/gpertea/gffread)
* Busco : Assessing Genomic Data Quality and Beyonde. (https://busco.ezlab.org/)
* tabix : Generic indexer for TAB-delimited genome position files (http://www.htslib.org/doc/tabix.html)
* R
* Bioperl

## Overview of programmed workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/umr_agap/workflows/funcannotate.git
  ```


- Change directories into the cloned repository:
  ```
  cd funcannotate
  ```

- Edit the configuration file config.yaml

```

# Genome to be annotated (Assembly, GFF3 and corresponding protein fasta file)
# Same keys for a genome, can be multiple
input_genomes:
  "PRC270":
    "assembly": "fasta/PRC270"
    "gff3": "fasta/PRC270.gff3"
    "protein": "fasta/PRC270_prot.fna"
    "jbrowse": "jbrowse_instance"
    "source": "eugene"
     
# Genome to be annotated (Assembly, GFF3 and corresponding protein fasta file)

#input_genomes:
#  "genomeA":
#    "assembly": "<path_to_assembly_fasta>"
#    "gff3": "<path_to_<gff3>"
#    "protein": "<path_to_protein_fasta>"
#    "source": "eugene" 


# Function transfer based on sequence similarity 
# The list is ordered according to the trust given to the database. 
# 
blast_databases:
  SwissProt : "/lustre/agap/BANK/uniprot/uniprot-swissprot_33090_20220607.fasta"
  TrEMBL: "/lustre/agap/BANK/uniprot/uniprot-trembl_33090_20220607.fasta"


#Should be download from https://busco-data.ezlab.org/v5/data/lineages/ and uncompress    
busco_path: "db/busco_downloads/"
lineage: "viridiplantae_odb10"

# Method for the protein similarity search (1 for diamond, 0 for blastp)
use_diamond: 1

# GFF3 Tag corresponding to sequence identifier (input_proteome)
# Allowed values are: ID, Name, locus_tag
# Require for GFF3 enrichment
tag: "Name"

# 0 For filter and check annotation quality ( filter transposable element genes and doubtful genes)
# 1 Only perform functional annotation (BLAST/Diamond + InterProScan)

only_reannotate: 1

# Rename gene id
rename_gene_id: 0

# 
blast_repeat_database:
  repbase: "db/repbase23.12_aaSeq_cleaned_2020.fa"

hmm_databases:
  Retro_gag: "db/Retrotran_gag_3.hmm"
  retro_gypsy: "db/Transposase_28_Gypsy.hmm"

keywork_to_remove:
  - "Retrotrans_gag"
  - "polyprotein"
  - "Gag.Pol"
  - "Gypsy"
  - "Copia"
  - "Reverse transcriptase"
  - "transpos"
  - "elomer"
  - "Elongation factor"
  - "EF-Ts"

ipro_term_to_remove:
  - "IPR000123"
  - "IPR015706"
  - "IPR013103"
  - "IPR003036"
  - "IPR002079"
  - "IPR004004"
  - "IPR003141"
  - "IPR005162"
  - "IPR000721"
  - "IPR014817"
  - "IPR014834"
  - "IPR016195"
  - "IPR015699"
  - "IPR004312"
  - "IPR001584"
  - "IPR004332"
  - "IPR004028"
  - "IPR004957"
  - "IPR003545"
  - "IPR018289"

# Split FASTA into 10 files (increase/decrease this value if need, 5000 sequence for each part is recommended) 
fasta_split_number: 10

# Threads
params:
  global_blast_threads: 4
  interproscan_threads: 4
  global_diamond_threads: 4
  busco_threads: 10 
  kofamscan_threads: 4

# Use envmodules (muse-login01)
modules:
  samtools: "samtools/1.14-bin"
  hmmer: "hmmer/3.3.2-bin"
  blast: "ncbi-blast/2.12.0+"
  interproscan: "interproscan/5.67-99.0"
  seqkit: "seqkit/2.0.0"
  diamond: "diamond/2.0.11"
  perllib: "perllib/5.16.3"
  gffread: "gffread/0.12.6"
  busco: "busco/5.0.0"
  R: "R/packages/4.1.0"
  python: "python/3.8.2"
  tabix: "htslib/1.16.0"
  meme: "meme/5.3.3"
  kofamscan: "kofamscan/1.3.0"
  agat: "/storage/replicated/cirad/binaries/ifb/modulefiles/agat/1.0.0


```

- Print shell commands to validate your modification

```
module load snakemake/7.15.1-conda
snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-envmodules --cores 1
```

- Run workflow on Meso@LR

```
sbatch snakemake.sh
```


## Output directory : 

All the output file name are prefixed with the key of input_genome parameter (i.e. : PRC270):

```shell
[drocg@muse-login01 funcannotate]$ tree output/
output/
├── PRC270.assembly.fna 
├── PRC270.cds.fna 
├── PRC270_dbxref.txt
├── PRC270_ec.txt
├── PRC270.gene.fna 
├── PRC270.gff3
├── PRC270.gff3.gz
├── PRC270.gff3.gz.tbi
├── PRC270_go.txt
├── PRC270_ipr.txt
├── PRC270_ko.txt 
├── PRC270.protein.faa    

```

 
