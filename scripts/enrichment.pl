#!/usr/bin/perl
use File::Basename;
use FindBin;
use Pod::Usage; 
use Getopt::Long;
use Data::Dumper;
use Cwd;
my $directory   = cwd();
my $verbose     = 0;
my $current_dir = getcwd;
my %ipr2go;
my %go2ipr;
my %exist;
my %list_gene;
my %list_go;
my %list_ipr;
my %list_dbxref;
my %list_ec;
my %list;
# List of file to remove
my @remove = (".log.txt",".sequence.txt",".xml.xml",".out.txt",".htmltarball.html.tar.gz",".gff.txt");

my $product = "";
my $output = "";
my $ko     = "";
my $result = "";

my $info_swissprot = "/lustre/agap/BANK/uniprot/uniprot-swissprot_33090_20220607.info";
my $info_trembl    = "/lustre/agap/BANK/uniprot/uniprot-trembl_33090_20220607.info";
my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters
       --product Product file [required]
       --ko      Blast Koala output
       --prefix  Output prefix
       --result  Directory containing all the Interpro output files [required]  
       --help    show this help and exit
       --outdir  Outdir  
/;
GetOptions(
     'product=s' => \$product,      
     'prefix=s'  => \$prefix,
     'result=s'  => \$result,
     'ko=s'      => \$ko,
     'outdir=s'  => \$directory,
     'help|h|?'  => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }

if ($product eq "") {
    warn "\nWarn :: --product is empty.\n\n";
    warn $usage;
    exit 0;
}      
 
my $go_file = $directory ."/". $prefix."_go.txt";
my $ipr_file = $directory ."/". $prefix."_ipr.txt";
my $ecnumber_file = $directory ."/". $prefix."_ec.txt";
my $dbxref_file  = $directory ."/".  $prefix."_dbxref.txt";
my $ko_file  = $directory ."/".  $prefix."_ko.txt";
# Download interpro2go
my $interpro2go = "interpro2go";
my $obo = "go.obo";
my $ko_list  = "ko_list";

my $url = "http://geneontology.org/external2go/interpro2go";
my $url_obo = "http://purl.obolibrary.org/obo/go.obo";
my $url_ko = "ftp://ftp.genome.jp/pub/db/kofam/ko_list.gz";

print "Download InterPro2GO file\n" if $verbose;
system("wget --quiet $url") unless -e $interpro2go; 

#system("wget --quiet $url_ko;gunzip $ko_list.gz") unless -e $ko_list;


# 
print "Download OBO file\n" if $verbose;
system("wget --quiet $url_obo") unless -e $obo;

print "Read OBO file\n" if $verbose;
open(IN,$obo);
my %go;
while(<IN>){
    chomp;
    if ($_ =~ /^id:\s(GO:\d+)/){
        $go{$1} = 1;
    }
}
close IN;

print "Read KO file\n" if $verbose;
open(IN,$ko_list);
my %ko;
my %info_ec;
while(<IN>){
    chomp;
    my ($id , $desc) = (split(/\t/,$_))[0,11];
	my $name;
	if ($desc =~ /(.*)\s\[EC:(.*)\]/) {
		$name = $1;
        
		my @ec_number = (split(/\s/,$2));
        if (scalar(@ec_number) > 0) {
            foreach my $ec_number(@ec_number) {
                push @{$info_ec{$id}}, $ec_number;
            }
        }
	}
}
close IN;
print "Read IPR2GO file\n" if $verbose;
# Create list between IPR id and GO id
open(IPR, $interpro2go);
while (<IPR>) {
    chomp;
	# InterPro:IPR000018 P2Y4 purinoceptor > GO:purinergic nucleotide receptor activity, G-protein coupled ; GO:0045028
    my ($ipr_id, $go_id )  = ($_ =~ /^InterPro:(IPR\d+)\s.*\s>\s.*\s;\s+(GO:\d+)/) ; 
    $ipr_id =~ s/InterPro\://;
    push @{$ipr2go{$ipr_id}} , $go_id; 
    push @{$go2ipr{$go_id}} , $ipr_id; 
}
close IPR;

my %info;
if (-e $info_swissprot) {
    print "Read SwissProt info file\n" if $verbose;
    open(IN,$info_swissprot);
    while(<IN>){
        chomp;
        my ($id,$source, $value) = (split(/\t/,$_))[0,1,2];
        push @{$info{$id}{$source}} , $value;
    }
    close IN;
}
if (-e $info_trembl){
    print "Read TrEMBL info file\n" if $verbose;
    open(IN,$info_trembl);
    while(<IN>){
        chomp;
        my ($id,$source, $value) = (split(/\t/,$_))[0,1,2];
        push @{$info{$id}{$source}} , $value;
    }
    close IN;
}
my %list_ko;

print "Parse output file\n" if $verbose;
open(IN,$ko);
while(<IN>){
    chomp;
    my ($gene_id,$ko) = (split(/\t/,$_))[0,1];
    if ($ko) { 
        push @{$list_ko{$gene_id}} , $ko;
        if ($info{$ko}){
            foreach my $ec (@{$info_ec{$ko}}){
                push @{$list_ec{$gene_id}} , $ec;
            }
        }
        
    }
}
close IN;
open(GO,">$go_file");
open(IPR,">$ipr_file");
open(Gene3D,">$ecnumber_file");
open(DBXREF, ">$dbxref_file");
open(KO, ">$ko_file");

print " Scan Directory and push all IPR id for each locus\n" if $verbose;
open(DIR,"ls $result/*.tsv |");
while (my $file = <DIR>) {
    chomp($file); 
    open(IN,$file);  
    while(<IN>) {
        chomp;
        my ($gene_id,$db,$value, $type,$ipr_id,$desc) = (split(/\t/,$_))[0,3,4,8,11,12];
        if ($ipr_id && $ipr_id ne "-") {
            if ($db eq "Gene3D") {
                $value = (split(/\:/,$value))[1];
                push @{$list_ec{$gene_id}} , $value;
            }
            if (defined $exist{$gene_id}{$ipr_id}){
                next;
            }
            else {
                $exist{$gene_id}{$ipr_id} = 1;
                push @{$list_ipr{$gene_id}} , $ipr_id;
                push @{$list_dbxref{$gene_id}} ,$db.":". $value if $value !~ /:/;
            }
        }
    }
    close IN;
}
close DIR;
  

foreach my $gene_id (keys %list_ipr) { 
    foreach my $dbxref_id (@{$list_dbxref{$gene_id}}){
        print DBXREF join("\t",$gene_id,$dbxref_id),"\n";
    }
    foreach my $ipr_id (@{$list_ipr{$gene_id}}){ 
        foreach my $go_id (@{$ipr2go{$ipr_id}}) {
            if (defined $exist{$gene_id}{$go_id}){
                next;
            }
            else {
                $exist{$gene_id}{$go_id} = 1;   
                push @{$list_go{$gene_id} } , $go_id;
            }
        }
    }
} 
my $count = 0;
open(IN,$product);
while(<IN>){
    chomp;
    my ($gene_id,$hit_name, $bank,$qlen, $function, $evidence_code ,$cds,$gene_name,$dbxref) = (split(/\t/,$_));
    if ($dbxref){
        $count++;
        my ($db,$accession) = (split(/\:/,$dbxref)); 
        if ($info{$accession}){
            foreach my $source (keys %{$info{$accession}}){
                if ($source eq "GO"){
                    foreach my $go_id (@{$info{$accession}{$source}}){ 
                        if (defined $exist{$gene_id}{$go_id}){
                            next;
                        }
                        else {
                            $exist{$gene_id}{$go_id} = 1;   
                            push @{$list_go{$gene_id} } , $go_id;
                        }
                    }
                }
                elsif ($source eq "InterPro") {
                    foreach my $ipr_id (@{$info{$accession}{$source}}){ 
                        if (defined $exist{$gene_id}{$ipr_id}){
                            next;
                        }
                        else {
                            $exist{$gene_id}{$ipr_id} = 1; 
                            push @{$list_ipr{$gene_id}} , $ipr_id; 
                        }
                    }
                    
                }
                elsif ($source eq "Gene3D"){
                    foreach my $value (@{$info{$accession}{$source}}){
                        if (defined $exist{$gene_id}{$value}){
                            next;
                        }
                        else {
                            $exist{$gene_id}{$value} = 1; 
                            push @{$list_ec{$gene_id}} , $value; 
                        }
                    }
                }
            }
        } 
    } 
}
foreach my $gene_id (keys %list_ipr ){
    foreach my $ipr_id (@{$list_ipr{$gene_id}}){
        print IPR join("\t",$gene_id, $ipr_id) ,"\n";
    }
}

foreach my $gene_id (keys %list_go ){
    foreach my $go_id (@{$list_go{$gene_id}}){ 
        if ($go{$go_id}){
            print GO join("\t",$gene_id, $go_id) ,"\n"; 
        }
    }
}
foreach my $gene_id (keys %list_ec ){
    foreach my $ec_id (@{$list_ec{$gene_id}}){ 
        print Gene3D join("\t",$gene_id, $ec_id) ,"\n"; 
    }
}

foreach my $gene_id (keys %list_ko ){
    foreach my $ko_id (@{$list_ko{$gene_id}}){ 
        print KO join("\t",$gene_id, $ko_id) ,"\n"; 
    }
}


close GO;
close IPR;
close Gene3D;
close DBXREF;
close KO;
system("rm $obo $interpro2go");


