#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

## Author : Alexandre Soriano
## This script uses a very naive approach. 

my $uniprot_file = "";
my $man = 0;
my $help = 0;
my $solid_evidence_only = 0;

GetOptions(
    "uniprot=s"          => \$uniprot_file,
    "solid-evidence!"    => \$solid_evidence_only,
    "help|?"             => \$help,
    "man"                => \$man
) or die("Error in command line arguments\n");

pod2usage(1) if $help;
pod2usage(-exitval => 0, -verbose => 2) if $man;

die "You must provide a UniProt .dat file." if $uniprot_file eq "";

# Evidence codes considered "solid"
my %solid_evidence = map { $_ => 1 } qw(EXP IDA IPI IMP IGI IEP TAS);
# my %solid_evidence = map { $_ => 1 } qw(EXP IDA IPI IMP IGI IEP TAS IC);


# Open the UniProt file for reading
open my $fh, '<', $uniprot_file or die "Cannot open file $uniprot_file: $!";

# Initialize variables to store information
my $org;
my $entry = "";
my @dbxref = ();
my @accession = ();
my $current_accession = "";

print "accession\tdatabase\tdbxref\n";

# Loop through the lines of the file
while (<$fh>) {
    chomp;

    # Get accession value
    if (/^AC\s+(\w+);/) {
        @accession = split /;/, $1;
        $current_accession = $accession[0];
        print STDERR "$current_accession\n";
        next;
    }

    # Parse database cross-references (e.g., GO terms, InterPro, Gene3D)
    elsif (/^DR\s+(.+)/) {
        my @dbxref = split /; /, $1;

        # Parse GO terms
        if ($dbxref[0] eq "GO") {
            my $evidence = $dbxref[3]; # Evidence code is typically the third field
            $evidence =~ s/:.*//;

            # print STDERR "$evidence\n";

            # Filter based on evidence codes if --solid-evidence is set
            if (!$solid_evidence_only || exists $solid_evidence{$evidence}) {
            # if (!$solid_evidence_only || (defined $evidence && $evidence =~ /^(\w+):/ && exists $solid_evidence{$1})) {
                print "$current_accession\t$dbxref[0]\t$dbxref[1]\n";
            }
        }

        # Parse InterPro and Gene3D terms
        elsif ($dbxref[0] eq "InterPro" || $dbxref[0] eq "Gene3D") {
            print "$current_accession\t$dbxref[0]\t$dbxref[1]\n";
        }
        next;
    }

    # Check if it's the end of the entry
    elsif (/^\/\/$/) {
        @dbxref = ();
        @accession = ();
        $current_accession = "";
        $entry = "";
    }
}

# Close the file
close $fh;

### END ###

=head1 NAME

parse_uniprot - parse UniProt .dat files to extract useful information for annotation.

=head1 SYNOPSIS

parse_uniprot -u /path/to/uniprot.dat [--solid-evidence]

 Options:
   -u               Path to UniProt .dat file
   --solid-evidence Filter GO terms to include only those with solid evidence
   -help            Print a brief help message and exits
   -man             Prints the manual page and exits

=head1 DESCRIPTION

This script parses UniProt .dat files to extract useful information, such as GO terms, InterPro domains, and Gene3D annotations. It includes an option to filter GO terms by solid evidence codes.

=head1 OPTIONS

=over 16

=item B<-uniprot>

UniProt .dat file.

=item B<--solid-evidence>

If set, only GO terms with solid evidence codes (EXP, IDA, IPI, IMP, IGI, IEP, TAS) are included.

=item B<-help>

Prints a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=cut